<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$facultate = "";


if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) ||  empty($check) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($firstname) < 3 || strlen($firstname) > 20 || strlen($lastname) < 3 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "First or Last name is shorter or longer than expected!";
}

if(strlen($question) < 15 ){
	$error = 1;
	$error_text = "Question is shorter than expected!";
}

if(!ctype_alpha($firstname) || !ctype_alpha($lastname) ){
	$error = 1;
	$error_text = "The First or Last name does not contain only letters";
}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $error = 1;
  $error_text = "Email is not a valid email address";
}

if(strlen($cnp) != 13 || $cnp[0] == "7" || $cnp[0] == "8" || $cnp[0] == "9" || $cnp[0] == "0"){
	$error = 1;
	$error_text = "CNP hasn't 13 numbers or the CNP doesn't start with 1, 2, 3, 4, 5 or 6";
}

if(!filter_var($facebook, FILTER_VALIDATE_URL)) {
	$error = 1;
	$error_text = "Facebook URL is nor a valid URL";
}

$birth = strtotime($birth);
$age_verification18 = strtotime('-18 years');
$age_verification100 = strtotime('-100 years');
if(!(date('d-m-Y', $birth) > date('d-m-Y', $age_verification18)) || !(date('d-m-Y', $birth) < date('d-m-Y', $age_verification100))){
	$error = 1;
	$error_text = "Age is not between 18 and 100";
}

if($captcha_generated != $captcha_inserted) {
	$error = 1;
	$error_text = "Captcha does not correspond";
}

if(strlen($facultate) < 3 || strlen($facultate) > 30 || (!ctype_alpha($facultate)){
	$error = 1;
	$error_text = "Facultate is shorter or longer than expected or it doesn't contain only letters!";
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}


